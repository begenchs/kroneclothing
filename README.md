# 👑 Krone Clothing is an application built on ReactJS

## 👨🏻‍💻 About the project

 This is an E-commerce application using React, Redux, React Router, Context API and Firebase. This is going to be a full stack app (MERN stack), using Firebase.

 https://coruscating-babka-79f452.netlify.app/

 - React

- React Router

- Redux

- Redux Saga

- Asynchronous Redux

- React Hooks

- Context API

- React Suspense + React Lazy

- Firebase

- Styled-Components

## 🚀 Frontend Technologies

Technologies that I used to develop this frontend app

- [ReactJS](https://reactjs.org)
- [Sass](https://sass-lang.com/)
- [Firebase](https://firebase.google.com/)
- [React Router](https://github.com/ReactTraining/react-router)
- [React Redux](https://react-redux.js.org/)
- [React Stripe](https://stripe.com/docs/stripe-js/react)
- [VS Code](https://code.visualstudio.com) with [ESLInt](https://eslint.org/docs/user-guide/getting-started), and [Prettier RC](https://github.com/prettier/prettier)

## 💻 Getting started

### Requirements

- [Node.js](https://nodejs.org/en/)
- [Yarn](https://classic.yarnpkg.com/) or [npm](https://www.npmjs.com/)


**Clone the project and access the folder**

```bash
$ git clone https://gitlab.com/begenchs/kroneclothing.git

$ cd kroneclothing

```

**Follow the steps below**

```bash
# Install the dependencies
$ yarn

# Run the web app
$ yarn dev
```

## 💻 Backend Technologies

Technologies that I used to develop this backend app
### Requirements

- [Node.js](https://nodejs.org/en/)
- [Yarn](https://classic.yarnpkg.com/) or [npm](https://www.npmjs.com/)
- [nodemon](https://nodemon.io/)
- [Express](https://expressjs.com/)
- [body-parser](https://github.com/expressjs/body-parser)
- [dotenv](https://github.com/motdotla/dotenv)

## Set your firebase config

Remember to replace the `config` variable in your `firebase.utils.js `with your own config object from the firebase dashboard! Navigate to the project settings and scroll down to the config code. Copy the object in the code and replace the variable in your cloned code.

## Set your stripe publishable key

Set the `publishableKey` variable in the `stripe-button.component.jsx` with your own publishable key from the stripe dashboard.

## 🤔 How to contribute 

**Follow the steps below**

```bash
# Clone your fork
$ git clone https://gitlab.com/begenchs/kroneclothing.git

$ cd crwn-clothing

# Create a branch with your feature
$ git checkout -b your-feature

# Make the commit with your changes
$ git commit -m 'feat: Your new feature'

# Send the code to your remote branch
$ git push origin your-feature
```

After your pull request is merged, you can delete your branch


Made with 👑 by Begench Saparov :wave: [Get in touch!](https://gitlab.com/begenchs) 
